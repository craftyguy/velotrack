MODULE := ridecasa

secrets:
	@echo "Generating app secrets..."
	@mkdir -p docker-compose/secrets
	@pwgen  -y -n 128 1 > docker-compose/secrets/ridecasa_app.txt

build:
	@cd docker-compose && docker-compose build --no-cache

run:
	@cd docker-compose && docker-compose up -d

lint:
	@echo "Running Flake8 against source and test files..."
	@flake8
	@echo "Running Bandit against source files..."
	@bandit -r --ini setup.cfg

clean:
	@cd docker-compose && docker-compose stop
	@cd docker-compose && docker-compose rm

css:
	@sassc sass/ridecasa.scss ridecasa/static/css/bulma.min.css

test:
	@python3 -m pytest --cov=ridecasa --cov-report=term-missing
