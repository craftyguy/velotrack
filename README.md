![image logo](./ridecasa/static/img/ridecasa_logo.png)

# Ride Casa

This is the source code for Ride Casa ([https://ride.casa](https://ride.casa))!

Ride Casa is a web application for analyzing and collecting activity
(bicycling, running, etc) tracks recorded in popular formats (e.g. `.tcx`).

Supported formats for importing activities/tracks are:

  * [TCX](https://en.wikipedia.org/wiki/Training_Center_XML)
  * FIT

This source code is provided under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)

[![builds.sr.ht status](https://builds.sr.ht/~craftyguy/ridecasa/.build.yml.svg)](https://builds.sr.ht/~craftyguy/ridecasa/.build.yml?)

## Installation / Running

### With Docker/Docker-Compose
If using the provided docker-compose configuration, the only requirements are
`docker` and `docker-compose`.

To run the build/run the containers:

```
$ make secrets

$ make build

$ make run
```

### Without Docker

To run without using docker (e.g. in a python virtualenv):

```
$ pip install -r requirements.txt

$ FLASK_APP="ridecasa:create_app('ridecasa.config.ProductionConfig')" flask run

```

Or if doing development:

```
$ FLASK_DEBUG=1 FLASK_APP="ridecasa:create_app('ridecasa.config.ProductionConfig')" flask run
```

The application uses a background task scheduler
([Huey](https://github.com/coleifer/huey)), it can be started with:

```
$ huey_consumer ridecasa.tasks.huey -w $(nproc) -k process
```

### Environment Variables

The following environment variables can be set to changed to control certain aspects of the application:

`RIDECASA_TMP_DIR`: Used to specify a directory to use for tmp, defaults to `/tmp` if unset.

`SQLITE_DB_DIR`: Location to store the sqlite database, defaults to `./models` if unset.

`RIDECASA_SQLITE_DB`: Filename for the sqlite db, which will be created under `SQLITE_DB_DIR` if it doesn't exist. Defaults to `ridecasa.db` if unset.

`WSGI_PROFILE`: Generate performance profile data for the application.

### Reporting Problems

Issues can be reported either on [the Ride Casa mailing
list](mailto:~craftyguy/ridecasa@lists.sr.ht?subject=%3Cbug%20report%3E) or by
filing an issue on the [Ride Casa TODO
tracker](https://todo.sr.ht/~craftyguy/ridecasa).

### Credits

This project uses the following software/libraries:

- [Bulma](https://bulma.io)
- [Leaflet](https://leafletjs.com)
- [Leaflet-GestureHandling](https://github.com/elmarquis/Leaflet.GestureHandling)
- [Flot](https://www.flotcharts.org)
- [Flask](https://flask.palletsprojects.com/en/1.1.x/)
- [PonyORM](https://ponyorm.org/)
- [Huey](https://github.com/coleifer/huey/)
- ... and a few smaller libraries in `requirements.txt`
