import bjoern
import ridecasa

bjoern.run(
    wsgi_app=ridecasa.create_app('ridecasa.config.ProductionConfig'),
    host='0.0.0.0',
    port=9000,
    reuse_port=True
)
