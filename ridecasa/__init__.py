import os
from flask import Flask, render_template
from flask_bcrypt import Bcrypt
from flask_caching import Cache
from flask_compress import Compress
from flask_login import LoginManager
from flask_talisman import Talisman
from .models import db, init_db

bcrypt = Bcrypt()
cache = Cache()
compress = Compress()
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
talisman = Talisman()
RIDECASA_SQLITE_DB = 'ridecasa.db'
if os.environ.get('SQLITE_DB_DIR'):
    os.makedirs(os.environ.get('SQLITE_DB_DIR'), exist_ok=True)
    RIDECASA_SQLITE_DB = os.path.join(os.environ.get('SQLITE_DB_DIR'),
                                      RIDECASA_SQLITE_DB)


def create_testing_app(config):
    app = Flask(__name__)
    app.debug = True
    app.config.from_object(config)
    login_manager.init_app(app)
    bcrypt.init_app(app)
    talisman.init_app(app, force_https=False)
    register_handlers(app, db)
    return register_blueprints(app)


def create_app(config):
    app = Flask(__name__)
    if os.environ.get('WSGI_PROFILE'):
        from werkzeug.middleware import profiler
        os.makedirs('profile', exist_ok=True)
        app.wsgi_app = profiler.ProfilerMiddleware(app.wsgi_app,
                                                   restrictions=[30],
                                                   profile_dir='profile')
        app.debug = True
    app.config.from_object(config)

    if os.environ.get('WSGI_PROFILE'):
        app.config['PROFILE'] = True

    login_manager.init_app(app)
    bcrypt.init_app(app)
    compress.init_app(app)
    cache.init_app(app)

    csp = {
        'default-src': '\'self\'',
        'img-src': [
            '\'self\' data:',
            '*.openstreetmap.org',
        ],
        'style-src': '\'unsafe-inline\' \'self\'',
        'script-src': '\'self\'',
        'base-uri': '\'self\'',
        'object-src': '\'none\'',
    }
    talisman.init_app(app, content_security_policy=csp,
                      content_security_policy_nonce_in=['script-src'])

    init_db(RIDECASA_SQLITE_DB)
    register_handlers(app, db)
    return register_blueprints(app)


def create_app_huey(config):
    app = Flask(__name__)
    if os.environ.get('WSGI_PROFILE'):
        from werkzeug.middleware import profiler
        os.makedirs('profile', exist_ok=True)
        app.wsgi_app = profiler.ProfilerMiddleware(app.wsgi_app,
                                                   restrictions=[30],
                                                   profile_dir='profile')
        app.debug = True
    app.config.from_object(config)

    if os.environ.get('WSGI_PROFILE'):
        app.config['PROFILE'] = True
    return app


def register_handlers(app, db):
    # register error handlers
    @app.errorhandler(500)
    def internal_error(error):
        # TODO: do logging of some sort
        db.rollback()
        return render_template('errors/500.html'), 500

    @app.errorhandler(404)
    def not_found_error(error):
        return render_template('errors/404.html'), 404


def register_blueprints(app):
    with app.app_context():
        from ridecasa.home.views import home_blueprint
        from ridecasa.auth.views import auth_blueprint
        from ridecasa.user.views import user_blueprint
        from ridecasa.activity.views import activity_blueprint
        from ridecasa.settings.views import settings_blueprint

        # Register Blueprints
        app.register_blueprint(home_blueprint)
        app.register_blueprint(auth_blueprint)
        app.register_blueprint(user_blueprint)
        app.register_blueprint(activity_blueprint)
        app.register_blueprint(settings_blueprint, url_prefix='/settings')

        return app
