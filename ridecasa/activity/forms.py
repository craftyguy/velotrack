from flask_wtf import FlaskForm
from wtforms import SelectField
from wtforms.validators import DataRequired


class ActivityChartAxisForm(FlaskForm):
    axis = SelectField('chart_axis', id='chartAxisSelect',
                       choices=[('time', 'time'), ('distance', 'distance')],
                       validators=[DataRequired()])

    def validate(self):
        valid = True
        if self.axis.data not in ['distance', 'time']:
            valid = False
        return valid
