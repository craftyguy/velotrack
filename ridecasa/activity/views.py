import itsdangerous
from average import EWMA
from flask import abort, Blueprint, current_app, redirect, render_template, request
from flask_login import current_user, login_required
from pony.orm import db_session
from pony import orm
from ridecasa.models import db
from ridecasa.sportdata import ActivitySummary, TrackData
from ridecasa import cache
from ridecasa.util import geohash_decode, haversine_distance_meters
from ridecasa.activity.forms import ActivityChartAxisForm


activity_blueprint = Blueprint('activity', __name__,
                               template_folder='templates')


@activity_blueprint.route('/activity/<int:activity_id>', methods=['GET', 'POST'])
@login_required
@db_session
def activity(activity_id):
    chart_axis_form = ActivityChartAxisForm()
    user_pref = orm.select(u for u in db.UserPref
                           if u.user == current_user).first()
    chart_axis = user_pref.activity_chart_axis
    if not chart_axis:
        chart_axis = 'distance'
    if request.method == 'POST':
        if 'shareButton' in request.form:
            token = request.form['shareButton']
            serializer = itsdangerous.serializer.Serializer(
                current_app.config['SECRET_KEY'])
            token_data = serializer.loads(itsdangerous.encoding.base64_decode(token))
            activity = db.Activity.get(activity_id=token_data['activity_id'])
            if activity.user != current_user:
                # POST to an activity not owned by current user
                abort(500)
            activity.public = not activity.public
        elif chart_axis_form.data and chart_axis_form.validate():
            if request.form['axis'] not in ['time', 'distance']:
                abort(500)
            chart_axis = request.form['axis']
            user_pref.activity_chart_axis = chart_axis

    return render_activity_page(current_user, activity_id, owned=True,
                                chart_axis=chart_axis)


@activity_blueprint.route('/activity/<token>', methods=['GET', 'POST'])
@db_session
def shared_activity(token):
    serializer = itsdangerous.serializer.Serializer(current_app.config['SECRET_KEY'])
    token_data = serializer.loads(itsdangerous.encoding.base64_decode(token))
    user = db.User.get(id=token_data['user_id'])
    if user == current_user:
        return redirect('/activity/' + str(token_data['activity_id']))

    # guard against displaying any activity that may have been shared
    # previously but is no longer
    activity_status = orm.select((a.public, a.public_token) for a in db.Activity
                                 if a.activity_id == token_data['activity_id']
                                 and a.user.id == token_data['user_id']).first()
    if not activity_status:
        abort(404)
    public, activity_token = activity_status
    if not public or activity_token != token:
        abort(404)

    chart_axis = 'distance'
    chart_axis_form = ActivityChartAxisForm()
    if request.method == 'POST':
        if chart_axis_form.data and chart_axis_form.validate():
            chart_axis = request.form['axis']

    return render_activity_page(user, token_data['activity_id'], owned=False,
                                chart_axis=chart_axis)


@db_session
def render_activity_page(user, activity_id, owned=False, chart_axis=None):  # noqa: C901
    try:
        int(activity_id)
    except ValueError:
        abort(404)
    if not db.Activity.exists(activity_id=activity_id):
        abort(404)

    # honor privacy geofence if user sharing activity has set one
    user_pref = orm.select(u for u in db.UserPref
                           if u.user == user).first()
    geofence_coord = None
    if user_pref.geofence_geohash:
        geofence_coord = geohash_decode(user_pref.geofence_geohash)
    geofence_radius_meters = user_pref.geofence_radius_meters
    # don't enable geofence if user owns activity
    geofence_enabled = user_pref.geofence_enabled and not owned

    activity = db.Activity.get(activity_id=activity_id)
    chart_axis_form = ActivityChartAxisForm()

    # shared activity should use geofence info for cache so it's invalid if
    # the geofence changes before the cache expires
    shared_key = ''
    if not owned:
        shared_key = str(geofence_coord) + str(geofence_radius_meters)
    cache_trackpoints_list_key = '_'.join(['trackpoints', user.username,
                                           str(activity_id), str(owned),
                                           shared_key])
    cache_tp_segments_key = '_'.join(['tp_segments', user.username,
                                      str(activity_id), str(owned), shared_key])
    cache_summary_key = '_'.join(['summary', user.username, str(activity_id),
                                  str(owned), shared_key])
    if activity.user != user:
        abort(404)
    # Note: most of the activity summary data is generated as trackpoints are
    # iterated below
    summary = cache.get(cache_summary_key)
    tp_cnt = 0
    speed_cnt = 0
    hr_cnt = 0
    cadence_cnt = 0
    prev_alt = 0
    tp_list = cache.get(cache_trackpoints_list_key)
    tp_segment_indexes = cache.get(cache_tp_segments_key)
    if not tp_list and not summary:
        tp_list = []
        tp_segment_indexes = set()
        idx = 0
        calories = int(activity.calories) if activity.calories else None
        summary = ActivitySummary(distanceMeters=round(activity.distanceMeters, 1),
                                  totalTimeSecs=0, startTime=activity.startTime,
                                  timezone=activity.timezone, sport=activity.sport,
                                  calories=calories)
        # collect trackpoints for activity. It's important that trackpoints are
        # sorted here since the activity summary is derived while parsing them,
        # and some attributes (like speed) depend on previous trackpoints
        trackpoints = activity.trackpoints.sort_by(db.TrackPoint.startOffsetSeconds)
        avg_altitude = EWMA(beta=0.99)
        cadence_ewma = EWMA(beta=0.85)
        # set to True in case the first trackpoint is in the geofence
        in_fence = True
        prev_in_fence = True
        for t in list(trackpoints):
            td = TrackData(startOffsetSeconds=t.startOffsetSeconds,
                           geohash=t.geohash, altitudeMeters=t.altitudeMeters,
                           distanceMeters=t.distanceMeters, heartrate=t.heartrate,
                           speed=round(t.speed, 2), cadence=t.cadence)
            if not summary.avg_speed and td.speed:
                summary.avg_speed = td.speed
            elif td.speed:
                summary.avg_speed = (((summary.avg_speed * speed_cnt) + td.speed)
                                     / (speed_cnt + 1))
            if not summary.avg_hr and td.heartrate:
                summary.avg_hr = td.heartrate
            elif td.heartrate:
                summary.avg_hr = (((summary.avg_hr * hr_cnt) + td.heartrate)
                                  / (hr_cnt + 1))
            if not summary.avg_cadence and td.cadence:
                summary.avg_cadence = td.cadence
            elif td.cadence:
                td.cadence = cadence_ewma.update(td.cadence)
                summary.avg_cadence = (((summary.avg_cadence * cadence_cnt)
                                        + td.cadence) / (cadence_cnt + 1))
            if td.speed:
                speed_cnt += 1
            if td.heartrate:
                hr_cnt += 1
            if td.cadence:
                cadence_cnt += 1
            alt = avg_altitude.update(td.altitudeMeters)
            if prev_alt:
                alt_diff = alt - prev_alt
                if alt_diff > 0:
                    summary.alt_gain += alt_diff
            prev_alt = alt
            tp_cnt += 1
            # round speed less than 1km/hr to 0 since speed calc isn't that
            # accurate anyways
            if td.speed < 1.0:
                td.speed = 0
            if td.speed > summary.max_speed:
                summary.max_speed = td.speed
            if td.heartrate > summary.max_hr:
                summary.max_hr = td.heartrate
            if summary.min_hr == 0 or td.heartrate < summary.min_hr:
                summary.min_hr = td.heartrate
            if summary.min_ele == 0 or td.altitudeMeters < summary.min_ele:
                summary.min_ele = td.altitudeMeters
            if summary.max_ele == 0 or td.altitudeMeters > summary.max_ele:
                summary.max_ele = td.altitudeMeters
            # save index for start of a track segment, for use when drawing
            # around the privacy geofence
            if geofence_enabled and geofence_coord:
                distance = haversine_distance_meters(geofence_coord, (td.lat, td.lon))
                in_fence = distance <= geofence_radius_meters
                if not in_fence and prev_in_fence:
                    # start of new track segment
                    tp_segment_indexes.add(idx)
                prev_in_fence = in_fence
                if in_fence:
                    continue
            tp_list.append(td)
            idx += 1

    # segment indexes need to be sorted so tracks are drawn correctly
    if tp_segment_indexes is None:
        # initialize so that jinja's tojson gives an empty array
        tp_segment_indexes = []
    else:
        tp_segment_indexes = list(tp_segment_indexes)
        tp_segment_indexes.sort()
    summary.avg_hr = round(summary.avg_hr)
    summary.avg_speed = round(summary.avg_speed)
    summary.avg_cadence = round(summary.avg_cadence)
    summary.totalTimeSecs = activity.totalTimeSecs
    show_hr_chart = summary.avg_hr != 0
    show_cadence_chart = summary.avg_cadence != 0
    # timeout = 1 day
    cache.set(cache_trackpoints_list_key, tp_list, timeout=86400)
    cache.set(cache_summary_key, summary, timeout=86400)
    cache.set(cache_tp_segments_key, tp_segment_indexes, timeout=86400)
    if not chart_axis:
        # default to using time for x-axis
        chart_axis = 'distance'
    chart_axis_form.axis.data = chart_axis
    return render_template('activity/index.html', trackpoints=tp_list,
                           tp_segment_indexes=tp_segment_indexes,
                           show_hr_chart=show_hr_chart,
                           show_cadence_chart=show_cadence_chart,
                           summary=summary,
                           is_public=activity.public,
                           public_token=activity.public_token, owned=owned,
                           geofence_coord=geofence_coord,
                           geofence_radius_meters=geofence_radius_meters,
                           geofence_enabled=geofence_enabled,
                           chart_axis_form=chart_axis_form, xaxis=chart_axis)
