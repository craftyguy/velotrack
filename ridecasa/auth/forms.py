from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo


class LoginForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired('Username required')])
    password = PasswordField('Password',
                             validators=[DataRequired('Password required')])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Log in')


class SignupForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired('Username required')])
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            EqualTo('repassword', message=('Passwords must match'))])
    repassword = PasswordField('Re-enter Password',
                               validators=[DataRequired()])
    email = StringField('E-mail', validators=[Email('Invalid e-mail address')])
    submit = SubmitField('Sign up')
