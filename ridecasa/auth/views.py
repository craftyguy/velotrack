from datetime import datetime
from flask import Blueprint, flash, redirect, render_template, request
from flask_login import login_required, login_user, logout_user
from pony.orm import db_session, flush
from ridecasa.auth.forms import LoginForm, SignupForm
from ridecasa.models import db
from ridecasa.util import safe_redirect
from ridecasa import bcrypt, login_manager

auth_blueprint = Blueprint('auth', __name__,
                           template_folder='templates')


@login_manager.user_loader
@db_session
def load_user(user_id):
    return db.User.get(id=user_id)


@auth_blueprint.route('/login', methods=['GET', 'POST'])
@db_session
def login():
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            username = form.username.data
            user = db.User.get(username=username)
            if (not user or not user.enabled
                    or not bcrypt.check_password_hash(user.password,
                                                      form.password.data)):
                flash('Invalid username or password.')
                return redirect('/login')
            flush()
            login_user(user, remember=form.data['remember_me'])
            user.last_login = datetime.utcnow()
            # redirect to url user originally requested
            return safe_redirect(request.args.get('next'))

    return render_template('auth/login.html', form=form, subtitle="Log In")


@auth_blueprint.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/login')


@auth_blueprint.route('/signup', methods=['GET', 'POST'])
@db_session
def signup():
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            username = form.username.data
            password = bcrypt.generate_password_hash(
                form.password.data).decode('utf-8')
            email = form.email.data
            # Do not allow duplicate usernames
            if db.User.get(username=username):
                flash('Username \'{}\' is already taken, please choose '
                      'another.'.format(username))
                return redirect('/signup')
            # Do not allow duplicate emails
            if db.User.get(email=email):
                flash('This email is already registered. Please log in '
                      'instead.')
                return redirect('/signup')
            user = db.User(username=username, password=password, email=email,
                           last_login=datetime.utcnow(), enabled=True)
            flush()
            db.UserPref(user=user)
            login_user(user)
            return redirect('/')

    return render_template('auth/signup.html', form=form, subtitle="Sign Up")
