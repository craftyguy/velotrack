import os


def get_app_version():
    """ derive version from git commit sha for current branch """
    version = 'unknown'
    if os.path.exists('.git/HEAD'):
        with open('.git/HEAD') as f:
            head = f.readline().rstrip().split()[-1]
        if os.path.exists('.git/' + head):
            with open('.git/' + head) as f:
                version = f.readline().rstrip()[:10]
    return version


def get_app_secret_key():
    secret = "CHANGE ME, SERIOUSLY!"  # nosec [B105] this isn't a real pass...
    lines = None
    if os.path.exists('/run/secrets/ridecasa_app_secret'):
        with open('/run/secrets/ridecasa_app_secret') as f:
            lines = f.readlines()
    if lines:
        secret = lines[0].rstrip()
    return secret


class Config():
    SECRET_KEY = get_app_secret_key()
    APP_VERSION = get_app_version()


class ProductionConfig(Config):
    CACHE_TYPE = 'simple'
    # https://flask-login.readthedocs.io/en/latest/#cookie-settings
    REMEMBER_COOKIE_SECURE = True
    REMEMBER_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_SAMESITE = 'Lax'
    SESSION_COOKIE_SAMESITE = 'Lax'
    SESSION_PROTECTION = 'strong'


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    # Bcrypt algorithm hashing rounds (reduced for testing purposes only!)
    BCRYPT_LOG_ROUNDS = 4
    WTF_CSRF_ENABLED = False
    CACHE_TYPE = 'null'
    CACHE_NO_NULL_WARNING = True
    SESSION_PROTECTION = None
