from flask_wtf import FlaskForm
from wtforms import SelectField


class ActivityFilterForm(FlaskForm):
    year = SelectField('Year', id='formYearSelect')
    month = SelectField('Month', id='formMonthSelect')
