from datetime import datetime
from dateutil.relativedelta import relativedelta
from flask import (Blueprint, make_response, redirect, render_template,
                   request)
from flask_login import current_user, login_required
from pony.orm import db_session
from pony import orm
from ridecasa.home.forms import ActivityFilterForm
from ridecasa.models import db
from ridecasa.util import get_user_prefs

home_blueprint = Blueprint('home', __name__)


@home_blueprint.route('/', methods=['GET', 'POST'])
@db_session
def summary():
    if not current_user.is_authenticated:
        return redirect('/login')

    prefs = get_user_prefs(current_user.id)
    # default to selecting current year/month
    selected_year = prefs.home_filter_selected_year
    if not selected_year:
        selected_year = str(datetime.utcnow().year)
    selected_month = prefs.home_filter_selected_month
    if not selected_month:
        selected_month = str(datetime.utcnow().month)

    form = ActivityFilterForm()
    if request.method == 'POST':
        selected_year = form.year.data
        selected_month = form.month.data
    # Fetch years from activities for filtering list of activities
    form.year.choices = orm.select((str(a.startTime.year),
                                    str(a.startTime.year)) for a
                                   in db.Activity
                                   if a.user.id == current_user.id)[:]
    form.year.choices = form.year.choices.to_list()
    form.year.choices.sort(reverse=True)
    # Add additional options to the menu
    form.year.choices = ([('all', 'All')] + form.year.choices
                         + [('1yr', '1 Year')] + [('6mo', '6 Months')]
                         + [('1mo', '1 Month')])
    # day, month, year
    chart_x_axis_unit = 'year'
    if selected_year == 'all':
        # select all activities
        activities_summary = orm.select((a.startTime.year, 1, 1,
                                         sum(a.distanceMeters))
                                        for a in db.Activity
                                        # sort by year only
                                        if a.user.id == current_user.id
                                        ).order_by(lambda a, y, m, d: y)
        activities = orm.select(a for a in db.Activity
                                if a.user.id == current_user.id)
        chart_x_axis_unit = 'year'
    # 1 year filter
    elif selected_year == '1yr':
        date_delta = datetime.utcnow() + relativedelta(months=-13)
        activities_summary = orm.select((a.startTime.year, a.startTime.month,
                                         1, sum(a.distanceMeters))
                                        for a in db.Activity
                                        if a.user.id == current_user.id and
                                        # sort by munging year and month
                                        a.startTime >= date_delta
                                        ).order_by(lambda a, y, m, d: y * 100 + m)
        activities = orm.select(a for a in db.Activity
                                if a.user.id == current_user.id and
                                a.startTime >= date_delta)
        chart_x_axis_unit = 'month'
    # 1 month filter
    elif selected_year == '1mo':
        date_delta = datetime.utcnow() + relativedelta(months=-1)
        activities_summary = orm.select((a.startTime.year, a.startTime.month,
                                         a.startTime.day, sum(a.distanceMeters))
                                        for a in db.Activity
                                        if a.user.id == current_user.id and
                                        # sort by munging month and day
                                        a.startTime >= date_delta
                                        ).order_by(lambda a, y, m, d: m * 100 + d)
        activities = orm.select(a for a in db.Activity
                                if a.user.id == current_user.id and
                                a.startTime >= date_delta)
        chart_x_axis_unit = 'day'
    # 6 month filter
    elif selected_year == '6mo':
        date_delta = datetime.utcnow() + relativedelta(months=-6)
        activities_summary = orm.select((a.startTime.year, a.startTime.month,
                                         1, sum(a.distanceMeters))
                                        for a in db.Activity
                                        if a.user.id == current_user.id and
                                        # sort by munging year and month
                                        a.startTime >= date_delta
                                        ).order_by(lambda a, y, m, d: y * 100 + m)
        activities = orm.select(a for a in db.Activity
                                if a.user.id == current_user.id and
                                a.startTime >= date_delta)
        chart_x_axis_unit = 'month'
    else:
        # Make sure that there are activities in the selected month for the
        # selected year
        if (selected_month == 'all' or selected_month not in
                orm.select(str(a.startTime.month) for a in db.Activity if
                           str(a.startTime.year) == selected_year)):
            # select only activities in the chosen year
            activities_summary = orm.select((a.startTime.year,
                                             a.startTime.month, 1,
                                             sum(a.distanceMeters))
                                            for a in db.Activity
                                            if a.user.id == current_user.id and
                                            # sort by munging year and month
                                            str(a.startTime.year) == selected_year
                                            ).order_by(lambda a, y, m, d: y * 100 + m)
            activities = orm.select(a for a in db.Activity
                                    if a.user.id == current_user.id and
                                    str(a.startTime.year) == selected_year)
            chart_x_axis_unit = 'month'
        else:
            # select only activities in the chosen year and month
            activities_summary = orm.select((a.startTime.year,
                                            a.startTime.month, a.startTime.day,
                                            sum(a.distanceMeters))
                                            for a in db.Activity
                                            if a.user.id == current_user.id and
                                            str(a.startTime.year) == selected_year and
                                            # sort by day only
                                            str(a.startTime.month) == selected_month
                                            ).order_by(lambda a, y, m, d: d)
            activities = orm.select(a for a in db.Activity
                                    if a.user.id == current_user.id and
                                    str(a.startTime.year) == selected_year and
                                    str(a.startTime.month) == selected_month)
            chart_x_axis_unit = 'day'
    # Sort and remove duplicates from the list of months.
    activity_months = orm.select(a.startTime for a in db.Activity if
                                 str(a.startTime.year) == selected_year and
                                 a.user.id == current_user.id)
    form.month.choices = sorted([(a.month,
                                  a.strftime('%b')) for a in
                                 activity_months], key=lambda x: x[0])
    form.month.choices = list(dict.fromkeys(form.month.choices))
    form.month.choices.insert(0, ('all', 'All'))
    # set the year and month as selected in the drop list
    form.month.data = selected_month
    form.year.data = selected_year

    total_summary = {
        'activities': len(orm.select(a for a in db.Activity
                                     if a.user.id == current_user.id)),
        'distanceMeters': orm.sum(a.distanceMeters for a in db.Activity
                                  if a.user.id == current_user.id),
        'time': orm.sum(a.totalTimeSecs for a in db.Activity
                        if a.user.id == current_user.id),
    }
    if total_summary['activities']:
        num_activities = len(orm.select(a for a in db.Activity
                                        if a.user.id == current_user.id))
        sum_speeds = orm.sum(a.avg_speed for a in db.Activity
                             if a.user.id == current_user.id)
        total_summary['speed'] = round(sum_speeds / num_activities, 1)
    else:
        total_summary['speed'] = 0.0

    resp = make_response(render_template('home/summary.html', form=form,
                                         activities=activities,
                                         total_summary=total_summary,
                                         activities_summary=activities_summary,
                                         chart_x_axis_unit=chart_x_axis_unit,
                                         selected_tab='summary'))
    prefs.home_filter_selected_year = selected_year
    prefs.home_filter_selected_month = selected_month
    return resp


@home_blueprint.route('/monthly', methods=['GET', 'POST'])
@db_session
@login_required
def monthly():
    years = orm.select(str(a.startTime.year) for a
                       in db.Activity
                       if a.user.id == current_user.id)[:]
    total_dicts = []

    months = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec')
    max_month_distance = 0
    for year in years:
        year_dict = {
            'year': year,
            'months': {},
            'total': 0,
        }
        activities_for_year = orm.select((a.startTime.month, a.distanceMeters)
                                         for a in db.Activity
                                         if a.user.id == current_user.id
                                         and a.startTime.year == str(year))[:]
        for activity in activities_for_year:
            total = round(activity[1] / 1000, 2)
            month = activity[0] - 1
            year_dict['total'] += total
            if month not in year_dict['months']:
                year_dict['months'][month] = total
            else:
                year_dict['months'][month] += total
            if year_dict['months'][month] > max_month_distance:
                max_month_distance = year_dict['months'][month]

        year_dict['total'] = round(year_dict['total'], 2)
        total_dicts.append(year_dict)
    total_dicts.sort(key=lambda x: int(x.get('year')), reverse=True)
    resp = make_response(render_template('home/monthly.html', form=None,
                                         total_dicts=total_dicts,
                                         months=months,
                                         max_month_distance=max_month_distance,
                                         selected_tab='monthly'))
    return resp
