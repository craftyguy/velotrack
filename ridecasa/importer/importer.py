import os
from ..parsers import FITParse, ParseError, TCXParse


class ActivityImportError(Exception):
    """Exception raised for errors when importing activity"""


class Importer():
    # Handlers for various supported sports tracker files. Value is a method
    # called to parse the given file.
    exthandlers = {
        '.fit': FITParse,
        '.tcx': TCXParse,
    }

    def __init__(self, file):
        self.file = file

    def import_file(self):
        data = []
        _, ext = os.path.splitext(self.file)
        if ext.lower() not in self.exthandlers:
            raise ActivityImportError(f"Unsupported extension '{ext}' for "
                                      f"file: {self.file}")
        parser = self.exthandlers[ext.lower()]()
        try:
            data.append(parser.parse(self.file))
        except ParseError as e:
            raise ActivityImportError(f"Error parsing file "
                                      f"'{self.file}': {e}")
        return data
