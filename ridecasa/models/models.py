import datetime
from flask_login import UserMixin
from pony import orm

db = orm.Database()


def init_db(database_location):
    try:
        db.bind('sqlite', database_location, create_db=True, timeout=10)
        db.generate_mapping(create_tables=True)
    except orm.core.BindingError:
        pass


class User(db.Entity, UserMixin):
    username = orm.Required(str, unique=True)
    password = orm.Required(str)
    email = orm.Required(str)
    enabled = orm.Required(bool)
    last_login = orm.Optional(datetime.datetime)
    activities = orm.Set('Activity')
    prefs = orm.Optional('UserPref', cascade_delete=True)
    msgs = orm.Set('UserMessage')


class UserPref(db.Entity):
    user = orm.Required(User, unique=True)
    default_sport = orm.Optional(str, default='Biking')
    geofence_enabled = orm.Optional(bool)
    geofence_geohash = orm.Optional(str)
    geofence_radius_meters = orm.Optional(float)
    home_filter_selected_month = orm.Optional(str)
    home_filter_selected_year = orm.Optional(str)
    activity_chart_axis = orm.Optional(str)


class UserMessage(db.Entity):
    msg_date = orm.Required(datetime.datetime)
    msg_type = orm.Required(str)
    msg = orm.Required(orm.LongUnicode)
    seen = orm.Required(bool, default=True)
    user = orm.Required(User)
    orm.composite_index(user, msg_type, msg)


class Activity(db.Entity):
    activity_id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str)
    sport = orm.Required(str)
    avg_speed = orm.Required(float)
    distanceMeters = orm.Required(float)
    # Note: startTime is localized to the timezone also stored here
    startTime = orm.Required(datetime.datetime)
    totalTimeSecs = orm.Required(float)
    timezone = orm.Required(str)
    creator = orm.Optional(str)
    calories = orm.Optional(float)
    avg_hr = orm.Optional(float)
    max_hr = orm.Optional(float)
    intensity = orm.Optional(str)
    trackpoints = orm.Set('TrackPoint')
    user = orm.Required(User)
    public = orm.Required(bool, default=False)
    public_token = orm.Optional(str)
    orm.composite_index(activity_id, user)
    orm.composite_index(startTime, user)
    orm.composite_index(startTime, user, sport)


class TrackPoint(db.Entity):
    geohash = orm.Required(str)
    heartrate = orm.Optional(int)
    cadence = orm.Optional(int)
    altitudeMeters = orm.Required(float)
    distanceMeters = orm.Required(float)
    speed = orm.Required(float)
    startOffsetSeconds = orm.Required(int)
    activity = orm.Required(Activity, index=True)
