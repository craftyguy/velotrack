class ParseError(Exception):
    """Exception raised for errors when parsing a file. """
