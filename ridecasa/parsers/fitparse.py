import fitparse
from ..sportdata import ActivityData, TrackData
from . import exceptions


class FITParse():

    def __init__(self):
        pass

    @staticmethod
    def semicircle_to_degrees(s):
        return s * 180 / (2**31)

    def parse(self, fit_file):
        try:
            ff_data = fitparse.FitFile(fit_file)
            ff_data.parse()
        except (fitparse.FitParseError, fitparse.utils.FitEOFError) as e:
            raise exceptions.ParseError("File is not in a valid FIT format: "
                                        f"{fit_file}: {e}")
        try:
            fit_summary = next(ff_data.get_messages('session'))
        except StopIteration as e:
            raise exceptions.ParseError("Unable to find a 'summary' message in FIT file: "
                                        f"{fit_file}: {e}")
        avg_speed = fit_summary.get_value('enhanced_avg_speed')
        if not avg_speed:
            avg_speed = fit_summary.get_value('avg_speed')
        # convert m/s to km/hr
        avg_speed *= 3.6
        # create an activity 'name' from the start date/time
        activity_name = fit_summary.get_value('start_time').strftime('%Y-%m-%dT%H:%M:%SZ')
        records = ff_data.get_messages('record')
        start_time = fit_summary.get_value('start_time')
        trackpoints = []
        for record in records:
            lat = record.get_value('position_lat')
            lon = record.get_value('position_long')
            if not lat or not lon:
                # ignore points with no position data
                continue
            r_time = record.get_value('timestamp')
            alt = record.get_value('enhanced_altitude')
            if not alt:
                alt = record.get_value('altitude')
            speed = record.get_value('enhanced_speed')
            if not speed:
                speed = record.get_value('speed')
            td = TrackData(
                startOffsetSeconds=int((r_time - start_time).total_seconds()),
                lat=self.semicircle_to_degrees(record.get_value('position_lat')),
                lon=self.semicircle_to_degrees(record.get_value('position_long')),
                altitudeMeters=round(alt, 2),
                distanceMeters=record.get_value('distance'),
                # convert m/s to km/hr
                speed=speed * 3.6,
            )
            heartrate = record.get_value('heart_rate')
            if heartrate:
                td.heartrate = heartrate
            cadence = record.get_value('cadence')
            if cadence:
                td.cadence = cadence
            trackpoints.append(td)
        # try to normalize activity sport with other file formats
        # TODO: maybe maintain some application-level mapping?
        sport = fit_summary.get_value('sport')
        if sport == 'cycling':
            sport = 'Biking'
        elif sport == 'running':
            sport = 'Running'
        activity = ActivityData(
            name=activity_name,
            sport=sport,
            trackpoints=trackpoints,
            distanceMeters=fit_summary.get_value('total_distance'),
            startTimeUTC=fit_summary.get_value('start_time'),
            totalTimeSecs=fit_summary.get_value('total_elapsed_time'),
            avg_speed=avg_speed,
            avg_hr=fit_summary.get_value('avg_heart_rate'),
            max_hr=fit_summary.get_value('max_heart_rate'),
            calories=fit_summary.get_value('total_calories'),
            intensity=0,  # TODO: not implemented
        )

        return [activity]
