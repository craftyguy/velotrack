from average import EWMA
from datetime import datetime
from lxml import objectify  # nosec (no alt. in defusedxml)
from ..sportdata import ActivityData, ActivitySummary, TrackData
from ..util import haversine_distance_meters
from . import exceptions

NAMESPACE = 'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2'


class TCXParse():

    def __init__(self):
        pass

    def _parse_track_points(self, tcx_trackpoints, start_time,
                            prev_speed_kmph=0):
        avg_spd = EWMA(beta=.85)
        prev_time = start_time
        trackpoints = []
        prev_distance = 0
        prev_latlon = ()
        for t in tcx_trackpoints:
            distance = 0.0
            lat = float(t.Position.LatitudeDegrees)
            lon = float(t.Position.LongitudeDegrees)
            if hasattr(t, 'DistanceMeters'):
                distance = float(t.DistanceMeters)
            elif prev_latlon:
                distance = haversine_distance_meters((lat, lon), prev_latlon)
            prev_latlon = (lat, lon)
            s_time = str(t.Time).replace(":", "-").replace('T', '-')[:-1].split("-")
            t_time = datetime(*map(int, s_time))
            if not start_time:
                # initialize to time of first trackpoint if not set
                start_time = t_time
                prev_time = start_time
            prev_secs = (t_time - prev_time).seconds
            speed_kmph = 0
            # prefer speed in trackpoint over calculating it here
            if hasattr(t, 'Extensions'):
                for ext in t.Extensions.getchildren():
                    if hasattr(ext, 'Speed'):
                        # multiply by 3.6 to convert m/s to km/hr
                        speed_kmph = ext.Speed * 3.6
                        break
            elif prev_secs and distance is not None:
                speed_kmph = ((distance - prev_distance)
                              * (3600 / prev_secs)) / 1000
                # Use acceleration to filter out speeds that are the result of
                # bad gps data
                accel = (((speed_kmph * 1000.0) - (prev_speed_kmph * 1000.0))
                         / (prev_secs * 3600))
                # 9 m/s^s may be too liberal, this may need to be adjusted if
                # it causes legit speeds to be filtered out. Also filter out
                # the first point in the activity since it's almost always bad.
                if ((accel > 9.0 and prev_speed_kmph != 0)
                        or prev_time == start_time):
                    speed_kmph = prev_speed_kmph
                speed_kmph = avg_spd.update(speed_kmph)
            start_offset = t_time - start_time
            td = TrackData(
                startOffsetSeconds=start_offset.seconds,
                lat=lat,
                lon=lon,
                altitudeMeters=float(t.AltitudeMeters),
                distanceMeters=distance,
                speed=speed_kmph,
            )
            if hasattr(t, 'HeartRateBpm'):
                try:
                    td.heartrate = float(t.HeartRateBpm.Value)
                except (ValueError, TypeError):
                    # Invalid HR value is ignored
                    pass
            trackpoints.append(td)
            prev_time = t_time
            prev_distance = distance
            prev_speed_kmph = speed_kmph
        return trackpoints, start_time, speed_kmph

    def _parse_tracks(self, tracks, start_time, prev_speed_kmph):
        total_trackpoints = []
        for track in tracks:
            if hasattr(track, 'Trackpoint'):
                tps, first_time, speed_kmph = self._parse_track_points(track.Trackpoint,
                                                                       start_time,
                                                                       prev_speed_kmph)
                total_trackpoints += tps
        return total_trackpoints, first_time, speed_kmph

    def _parse_laps(self, tcx_laps, prev_speed_kmph):
        total_distance = 0.0
        activity_start_time = None
        total_time_secs = 0.0
        total_trackpoints = []
        avg_hr = 0
        hr_cnt = 0
        hr_total = 0
        max_hr = 0
        avg_speed = 0.0
        for tcx_lap in tcx_laps:
            lap_start_time = None
            distance = float(tcx_lap.DistanceMeters)
            total_distance += distance
            lap_secs = float(tcx_lap.TotalTimeSeconds)
            if hasattr(tcx_lap, 'StartTime'):
                s_lap_time = str(tcx_lap.attrib['StartTime']
                                 ).replace(":", "-").replace('T', '-')[:-1].split("-")
                lap_start_time = datetime(*map(int, s_lap_time))
            total_time_secs += lap_secs
            if not activity_start_time and lap_start_time:
                activity_start_time = lap_start_time
            # note: tcx laps can have one or more 'tracks' in them, each with
            # lists of trackpoints
            # TCX with 'Course' layout will not have a 'Track' child
            if hasattr(tcx_lap, 'Track'):
                (tps, first_tp_time,
                 prev_speed_kmph) = self._parse_tracks(tcx_lap.Track,
                                                       activity_start_time,
                                                       prev_speed_kmph)
                total_trackpoints += tps
                # set activity start time to first trackpoint time
                if not activity_start_time:
                    activity_start_time = first_tp_time
            if hasattr(tcx_lap, 'AverageHeartRateBpm'):
                hr = 0
                if hasattr(tcx_lap.AverageHeartRateBpm, 'Value'):
                    hr = float(tcx_lap.AverageHeartRateBpm.Value)
                else:
                    hr = float(tcx_lap.AverageHeartRateBpm)
                if hr:
                    hr_cnt += 1
                    hr_total += hr
            if hasattr(tcx_lap, 'MaximumHeartRateBpm'):
                hr = 0
                if hasattr(tcx_lap.MaximumHeartRateBpm, 'Value'):
                    hr = float(tcx_lap.MaximumHeartRateBpm.Value)
                else:
                    hr = float(tcx_lap.MaximumHeartRateBpm)
                if hr and hr > max_hr:
                    max_hr = hr
        if hr_cnt and hr_total:
            avg_hr = hr_total // hr_cnt
        avg_speed = (total_distance * (3600 / total_time_secs)) / 1000
        return (total_trackpoints,
                ActivitySummary(avg_hr=avg_hr, max_hr=max_hr,
                                startTime=activity_start_time,
                                totalTimeSecs=total_time_secs,
                                distanceMeters=total_distance,
                                avg_speed=avg_speed))

    def parse(self, tcx_file):
        activities = []
        # prev_speed_kmph used to provide consistency between laps and sets of
        # trackpoints
        prev_speed_kmph = 0
        try:
            tcx = objectify.parse(tcx_file).getroot()
        except Exception as e:
            raise exceptions.ParseError("File is not in a valid TCX format: "
                                        f"{tcx_file}: {e}")
        activities_attr = 'Activities'
        if hasattr(tcx, 'Courses'):
            activities_attr = 'Courses'
        for tcx_activity_root in getattr(tcx, activities_attr):
            activity_attr = 'Activity'
            if hasattr(tcx_activity_root, 'Course'):
                activity_attr = 'Course'
            tcx_activity = getattr(tcx_activity_root, activity_attr)
            # Add trackpoints to activity
            try:
                trackpoints, summary = self._parse_laps(tcx_activity.Lap,
                                                        prev_speed_kmph)
            except Exception as e:
                raise exceptions.ParseError("Error parsing TCX file: "
                                            f"{tcx_file}: {e}")
            if not trackpoints and hasattr(tcx_activity, 'Track'):
                # 'Course' layout in TCX, 'Track' is separate
                trackpoints, summary.startTime, _ = self._parse_tracks(tcx_activity.Track,
                                                                       summary.startTime,
                                                                       0.0)
            if not trackpoints:
                raise exceptions.ParseError("Error parsing TCX file: "
                                            "Unable to find any tracks/trackpoints")
            # default to naming the activity after the start date/time
            activity_name = summary.startTime.strftime('%Y-%m-%dT%H:%M:%SZ')
            if hasattr(tcx_activity, 'Name'):
                activity_name = str(tcx_activity.Name)
            elif hasattr(tcx_activity, 'Id'):
                activity_name = str(tcx_activity.Id)
            sport = None
            if hasattr(tcx_activity, 'Sport'):
                sport = str(tcx_activity.attrib['Sport'])
            activity = ActivityData(
                name=activity_name,
                sport=sport,
                trackpoints=trackpoints,
                distanceMeters=summary.distanceMeters,
                startTimeUTC=summary.startTime,
                avg_speed=summary.avg_speed,
                avg_hr=summary.avg_hr,
                max_hr=summary.max_hr,
                totalTimeSecs=summary.totalTimeSecs,
                calories=0,  # TODO: not implemented
                intensity=0,  # TODO: not implemented
            )
            if hasattr(tcx_activity, 'Creator'):
                activity.creator = str(tcx_activity.Creator.Name)
            activities.append(activity)
        return activities
