from flask_wtf import FlaskForm
from wtforms import HiddenField, PasswordField, StringField, SubmitField
from wtforms.fields.html5 import DecimalRangeField
from wtforms.validators import AnyOf, Email, EqualTo, Optional, DataRequired
from ridecasa.util import geohash_encode, GeohashEncodeException, remove_parens


class SettingsSecurityForm(FlaskForm):
    new_password = PasswordField(
        'New Password',
        validators=[Optional()])
    repassword = PasswordField('Re-enter Password',
                               validators=[
                                   EqualTo('new_password',
                                           message=('Passwords must match'))])
    current_password = PasswordField(
        'Current Password',
        validators=[Optional()])
    email = StringField('E-mail', validators=[Email('Invalid e-mail address')])
    submit = SubmitField('Save')

    def validate(self):
        valid = True
        if not FlaskForm.validate(self):
            valid = False
        if self.new_password.data and not self.current_password.data:
            self.current_password.errors.append("Current password is "
                                                "required to change "
                                                "password.")
            valid = False
        else:
            return valid


class SettingsDeleteUserForm(FlaskForm):
    current_password = PasswordField(
        'Current Password',
        validators=[DataRequired()])
    i_am_sure = StringField('Type "I AM SURE"',
                            validators=[DataRequired(),
                                        AnyOf(['I AM SURE'],
                                              message='You must enter "I AM SURE"')
                                        ])
    submit = SubmitField('Delete Account')


class SettingsPrivacyGeofenceForm(FlaskForm):
    # in km
    geofence_radius = DecimalRangeField('Radius of geofence')
    geofence_coord = HiddenField()
    submit_geofence = SubmitField('Save Geofence')
    remove_geofence = SubmitField('Remove Geofence')

    def validate(self):
        valid = True
        if not self.geofence_radius.data or not self.geofence_coord.data:
            valid = False
        try:
            lat, lng = remove_parens(self.geofence_coord.data).split(',')
            geohash_encode(float(lat), float(lng))
        except (SyntaxError, GeohashEncodeException) as e:
            self.geofence_coord.errors.append("There was a failure when using the "
                                              f"selected point on the map: {e}")
            valid = False
        try:
            radius = float(self.geofence_radius.data)
            if radius < 0.0 or radius > 5.0:
                self.geofence_radius.errors.append("There was a failure when using the "
                                                   "selected radius. Try again.")
                valid = False
        except Exception as e:
            self.geofence_radius.errors.append("There was a failure when using the "
                                               f"selected radius. {e}")
            valid = False

        return valid
