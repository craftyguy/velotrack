import itsdangerous
from flask import (abort, Blueprint, current_app, flash, redirect, request,
                   render_template)
from flask_login import (current_user, login_required, logout_user)
from pony.orm import db_session
from pony import orm
from ridecasa.models import db
from ridecasa import bcrypt
from ridecasa.settings.forms import (SettingsSecurityForm, SettingsDeleteUserForm,
                                     SettingsPrivacyGeofenceForm)
from ridecasa.tasks import delete_user
from ridecasa.util import geohash_decode, geohash_encode, remove_parens


settings_blueprint = Blueprint('settings', __name__,
                               template_folder='templates')


@settings_blueprint.route('/privacy', methods=['GET', 'POST'])
@db_session
@login_required
def privacy():
    shared_activities = orm.select(a for a in db.Activity
                                   if a.user == current_user
                                   and a.public is True)
    user_pref = orm.select(u for u in db.UserPref
                           if u.user == current_user).first()

    geofence_geohash = user_pref.geofence_geohash
    geofence_radius_meters = user_pref.geofence_radius_meters
    geofence_enabled = user_pref.geofence_enabled

    if not geofence_radius_meters:
        geofence_radius_meters = 100
    geofence_coord = None
    if geofence_geohash:
        geofence_coord = geohash_decode(geofence_geohash)
    geofence_form = SettingsPrivacyGeofenceForm(coord=str(geofence_coord),
                                                radius=geofence_radius_meters / 1000)
    if request.method == 'POST':
        # user clicked 'unshare' on an activity
        if 'shareButton' in request.form:
            token = request.form['shareButton']
            serializer = itsdangerous.serializer.Serializer(current_app.config['SECRET_KEY'])  # noqa: E501
            token_data = serializer.loads(itsdangerous.encoding.base64_decode(token))
            activity = db.Activity.get(activity_id=token_data['activity_id'])
            if activity.user != current_user:
                # POST to an activity not owned by current user
                abort(500)
            activity.public = False
        # save geofence was submitted
        elif ('submit_geofence' in request.form and geofence_form.data
                and geofence_form.validate()):
            lat, lng = remove_parens(geofence_form.geofence_coord.data).split(',')
            geofence_coord = (float(lat), float(lng))
            ghash = geohash_encode(geofence_coord[0], geofence_coord[1])
            geofence_radius_meters = float(geofence_form.geofence_radius.data * 1000)
            user_pref.geofence_radius_meters = geofence_radius_meters
            user_pref.geofence_geohash = ghash
            user_pref.geofence_enabled = True
        # button to remove geofence was submitted
        elif 'remove_geofence' in request.form:
            geofence_coord = ''
            geofence_enabled = False
            # default to 0.1km
            geofence_radius_meters = 100
            user_pref.geofence_radius_meters = geofence_radius_meters
            user_pref.geofence_geohash = ''
            user_pref.geofence_enabled = False

    return render_template('settings/privacy.html', selected_tab='privacy',
                           geofence_form=geofence_form,
                           geofence_coord=geofence_coord,
                           geofence_radius_meters=geofence_radius_meters,
                           geofence_enabled=geofence_enabled,
                           shared_activities=shared_activities)


@settings_blueprint.route('/security', methods=['GET', 'POST'])
@db_session
@login_required
def security():
    form = SettingsSecurityForm(email=current_user.email)
    if request.method == 'POST':
        email = form.email.data
        if not form.validate_on_submit():
            return render_template('settings/security.html', form=form,
                                   selected_tab='security')
        if (orm.select(u for u in db.User if u.email == email).limit(1)
                and email != current_user.email):
            flash('This email address is already registered with an '
                  'account here.', 'error')
            return render_template('settings/security.html', form=form,
                                   selected_tab='security')
        if email != current_user.email:
            current_user.email = email
            flash('Email address updated', 'success')
        if form.new_password.data:
            new_pass = bcrypt.generate_password_hash(
                form.new_password.data).decode('utf-8')
            if not bcrypt.check_password_hash(current_user.password,
                                              form.current_password.data):
                flash('Current password is incorrect.', 'error')
                return render_template('settings/security.html', form=form,
                                       selected_tab='security')
            current_user.password = new_pass
            flash('Password updated', 'success')

    return render_template('settings/security.html', form=form,
                           selected_tab='security')


@settings_blueprint.route('/general', methods=['GET', 'POST'])
@db_session
@login_required
def general():
    form = SettingsDeleteUserForm()
    if request.method == 'POST':
        if not form.validate_on_submit():
            return render_template('settings/general.html', form=form,
                                   selected_tab='general')

        if not bcrypt.check_password_hash(current_user.password,
                                          form.current_password.data):
            flash('Current password is incorrect.', 'error')
            return render_template('settings/general.html', form=form,
                                   selected_tab='general')

        # schedule task to delete user later
        delete_user(current_user.id)
        # mark user as not enabled, since delete may not be instant
        user = db.User.get(id=current_user.id)
        user.enabled = False

        logout_user()
        return redirect('/login')
    return render_template('settings/general.html', form=form,
                           selected_tab='general')
