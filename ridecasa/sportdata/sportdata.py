import attr
import datetime
import typing
from dateutil import tz
from timezonefinder import TimezoneFinder
from ..util import geohash_encode, geohash_decode


@attr.s(auto_attribs=True, slots=True)
class ActivitySummary():
    avg_speed: float = attr.ib(default=0.0)
    max_speed: float = attr.ib(default=0.0)
    avg_hr: int = attr.ib(default=0)
    max_hr: int = attr.ib(default=0)
    min_hr: int = attr.ib(default=0)
    avg_cadence: int = attr.ib(default=0)
    alt_gain: float = attr.ib(default=0.0)
    min_ele: float = attr.ib(default=0.0)
    max_ele: float = attr.ib(default=0.0)
    totalTimeSecs: float = attr.ib(default=0)
    distanceMeters: float = attr.ib(default=0.0)
    calories: int = attr.ib(default=None)
    startTime: datetime.datetime = attr.ib(default=None)
    timezone: str = attr.ib(default=None)
    sport: str = attr.ib(default=None)


@attr.s(auto_attribs=True, slots=True)
class TrackData():
    startOffsetSeconds: int
    altitudeMeters: float
    distanceMeters: float
    speed: float
    heartrate: int = attr.ib(converter=int, default=0)
    cadence: int = attr.ib(default=0)
    geohash: str = attr.ib(default=None)
    lat: float = attr.ib(default=None)
    lon: float = attr.ib(default=None)

    def __attrs_post_init__(self):
        if self.geohash:
            self.lat, self.lon = geohash_decode(self.geohash)
        elif self.lon and self.lat:
            self.geohash = geohash_encode(lat=self.lat, lon=self.lon)


@attr.s(auto_attribs=True, slots=True)
class ActivityData():
    name: str
    sport: str
    avg_speed: float
    calories: float
    distanceMeters: float
    intensity: str
    totalTimeSecs: float
    trackpoints: typing.List[TrackData]
    creator: str = None
    max_hr: int = attr.ib(default=0)
    avg_hr: int = attr.ib(default=0)
    startTimeUTC: datetime.datetime = attr.ib(default=None)
    startTime: datetime.datetime = attr.ib(default=None)
    timezone: str = attr.ib(default=None)

    def __attrs_post_init__(self):
        # Derive activity timezone from first trackpoint
        if not self.timezone:
            tf = TimezoneFinder()
            first_tp = self.trackpoints[0]
            self.timezone = tf.timezone_at(lat=first_tp.lat,
                                           lng=first_tp.lon)
        local_tzinfo = tz.gettz(self.timezone)
        # Derive start time in UTC from a given start time that is localized to
        # the given timezone
        if not self.startTimeUTC and self.startTime:
            startTime = self.startTime.replace(tzinfo=local_tzinfo)
            self.startTimeUTC = startTime.astimezone(tz=tz.UTC)
            # Strip off tz info so orm 'stores' this as a datetime and not
            # a str
            self.startTimeUTC = self.startTimeUTC.replace(tzinfo=None)
        # Derive start time from a given start time in UTC and the activity's
        # timezone
        if not self.startTime and self.startTimeUTC:
            startTimeUTC = self.startTimeUTC.replace(tzinfo=tz.UTC)
            self.startTime = startTimeUTC.astimezone(tz=local_tzinfo)
            # Strip off tz info so orm 'stores' this as a datetime and not
            # a str
            self.startTime = self.startTime.replace(tzinfo=None)
