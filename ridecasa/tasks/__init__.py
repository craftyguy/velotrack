import itsdangerous
import os
from flask import current_app
from pony.orm import db_session
from pony import orm
from .config import huey
import ridecasa
from ridecasa.importer import ActivityImportError, Importer
from ridecasa.sportdata import ActivitySummary
from ridecasa.util import add_user_msg, fast_round, UserMessageType
from ridecasa import create_app_huey, db
from ridecasa.models import init_db


def add_trackpoints(trackpoints, activity_id):
    total_time = 0
    avg_hr = 0
    max_hr = 0
    for t in trackpoints:
        db.TrackPoint(activity=activity_id, geohash=t.geohash,
                      heartrate=int(t.heartrate), altitudeMeters=t.altitudeMeters,
                      distanceMeters=fast_round(t.distanceMeters, 2),
                      startOffsetSeconds=t.startOffsetSeconds,
                      speed=fast_round(t.speed, 2), cadence=int(t.cadence))
        total_time = t.startOffsetSeconds
    return ActivitySummary(totalTimeSecs=total_time, avg_hr=avg_hr,
                           max_hr=max_hr)


def delete_activity(activity_id):
    # Bulk delete trackpoints
    orm.delete((t for t in db.TrackPoint
                if t.activity.activity_id == activity_id))
    # Finally delete activity
    db.Activity[activity_id].delete()


@huey.on_startup()
def huey_startup():
    init_db(ridecasa.RIDECASA_SQLITE_DB)


@huey.task(priority=0)
@db_session
def delete_user(current_user_id):
    orm.delete(u for u in db.User
               if u.id == current_user_id)


@huey.task(priority=10)
def import_file(filename, current_user_id, overwrite_existing=False):
    app = create_app_huey('ridecasa.config.ProductionConfig')
    print(f"Importing file {os.path.basename(filename)}")
    with app.app_context():
        i = Importer(filename)
        try:
            file_contents = i.import_file()
        except ActivityImportError as e:
            add_user_msg(current_user_id, UserMessageType.ERROR,
                         f"Unable to import file: {e}")
            return
        for activities in file_contents:
            for activity in activities:
                add_activity(activity, current_user_id,
                             overwrite_existing=overwrite_existing)
        os.remove(filename)
        # clean up temp dir if this was the last file in it
        if not any(os.scandir(os.path.dirname(filename))):
            os.rmdir(os.path.dirname(filename))


@db_session(retry=5)
def add_activity(activity, current_user_id, overwrite_existing=False):
    existing = db.Activity.get(name=activity.name, user=current_user_id)
    if existing:
        if overwrite_existing:
            add_user_msg(current_user_id, UserMessageType.INFO,
                         f'Activity exists, replacing: {activity.name}')
            delete_activity(existing.activity_id)
        else:
            add_user_msg(current_user_id, UserMessageType.INFO,
                         f'Activity exists, *not* replacing: {activity.name}')
            return
    user_id = current_user_id
    sport = activity.sport
    if not sport:
        user_pref = orm.select(u for u in db.UserPref
                               if u.user.id == current_user_id).first()
        if user_pref.default_sport:
            sport = user_pref.default_sport
        else:
            sport = 'Biking'
    new_activity = db.Activity(name=activity.name, sport=sport,
                               creator=(activity.creator or ''),
                               distanceMeters=fast_round(activity.distanceMeters, 2),
                               startTime=activity.startTime,
                               totalTimeSecs=0.0,
                               timezone=activity.timezone,
                               user=user_id,
                               avg_speed=fast_round(activity.avg_speed, 2),
                               calories=activity.calories)
    # commit to get activity_id
    db.flush()
    summary = add_trackpoints(activity.trackpoints,
                              new_activity.activity_id)
    new_activity.totalTimeSecs = summary.totalTimeSecs
    new_activity.avg_hr = summary.avg_hr
    new_activity.max_hr = summary.max_hr

    # generate activity/user token
    serializer = itsdangerous.serializer.Serializer(current_app.config['SECRET_KEY'])
    public_token = itsdangerous.encoding.base64_encode(serializer.dumps({
        'user_id': current_user_id,
        'activity_id': new_activity.activity_id
    }))
    new_activity.public_token = public_token.decode()
    add_user_msg(current_user_id, UserMessageType.INFO,
                 f'Added activity {activity.name}')
