import os
from huey import SqliteHuey

HUEY_SQLITE_DB = 'huey.db'
if os.environ.get('SQLITE_DB_DIR'):
    os.makedirs(os.environ.get('SQLITE_DB_DIR'), exist_ok=True)
    HUEY_SQLITE_DB = os.path.join(os.environ.get('SQLITE_DB_DIR'),
                                  HUEY_SQLITE_DB)
huey = SqliteHuey(filename=HUEY_SQLITE_DB)
