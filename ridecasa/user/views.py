import os
import tempfile
from flask import (Blueprint, redirect, render_template,
                   request)
from flask_login import current_user
from pony.orm import db_session
from pony import orm
from flask_login import login_required
from werkzeug.utils import secure_filename
from ridecasa.tasks import import_file
from ridecasa.util import extract_archive
from ridecasa.models import db

user_blueprint = Blueprint('user', __name__,
                           template_folder='templates')


@user_blueprint.route('/upload', methods=['POST'])
@login_required
def file_upload():
    # allow setting a subdir in /tmp for cases where importer and web backend
    # are different containers
    if (tmp_dir := os.environ.get('RIDECASA_TMP_DIR')) is None:
        tmp_dir = '/tmp'
    dest_dir = tempfile.mkdtemp(prefix=f"{tmp_dir}/tmp_ridecasa_{current_user.id}")

    # handle multiple files, or one
    files = []
    overwrite = True if request.form.get('overwrite') == 'True' else False
    for file in request.files.getlist('file'):
        filename = os.path.join(dest_dir, (str(current_user.id) + '_'
                                           + secure_filename(file.filename)))
        file.save(filename)
        files += extract_archive(filename, dest_dir)
    for file in files:
        import_file(file, current_user.id, overwrite)

    return redirect('/')


@user_blueprint.route('/messages')
@db_session
@login_required
def messages():
    msgs = orm.select(m for m in db.UserMessage
                      if m.user.id == current_user.id
                      ).order_by(orm.desc(db.UserMessage.msg_date))
    return render_template('user/messages.html', msgs=msgs)
