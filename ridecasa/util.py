import geohash
import os
import re
import zipfile
from datetime import datetime
from enum import Enum
from flask import (redirect, request)
from math import asin, cos, radians, sin, sqrt
from pony.orm import db_session
from pony import orm
from urllib.parse import urljoin, urlparse
from .models import db


class GeohashEncodeException(Exception):
    """Exception raised for errors when encoding a geohash. """


class GeohashDecodeException(Exception):
    """Exception raised for errors when decoding a geohash. """


def geohash_encode(lat, lon):
    try:
        return geohash.encode(lat, lon)
    except Exception as e:
        raise GeohashEncodeException(f"Failed to encode geohash: {e}")


def geohash_decode(gh):
    try:
        return geohash.decode(gh)
    except Exception as e:
        raise GeohashDecodeException(f"Failed to decode geohash: {e}")


class UserMessageType(Enum):
    ERROR = "error"
    INFO = "info"


@db_session
def add_user_msg(user_id, msg_type, msg):
    db.UserMessage(user=user_id, msg_type=msg_type.value, msg=msg,
                   msg_date=datetime.utcnow())
    db.commit()


def extract_archive(archive, dest):
    if not os.path.exists(dest):
        os.makedirs(dest)
    _, ext = os.path.splitext(archive)
    files = []
    if ext == '.zip':
        with zipfile.ZipFile(archive, 'r') as f:
            f.extractall(dest)
            files = [os.path.join(dest, m) for m in f.namelist()]
            os.remove(archive)
    else:
        files = [os.path.join(dest, archive)]
    return files


# from http://flask.pocoo.org/snippets/63/
def is_url_safe(url):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, url))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


def safe_redirect(url):
    """ Redirect to url, or to '/' if not safe """
    if url and is_url_safe(url):
        return redirect(url)
    return redirect('/')


@db_session
def get_user_prefs(user_id):
    return orm.get(p for p in db.UserPref if p.user.id == user_id)


def remove_parens(s):
    return re.sub(r'[()]', '', str(s))


def haversine_distance_meters(latlon1, latlon2):
    # avg Earth radius from https://en.wikipedia.org/wiki/Earth_radius
    r = 6371000

    # formula from https://en.wikipedia.org/wiki/Haversine_formula
    lat1, lon1 = latlon1
    lat2, lon2 = latlon2
    lat1_r = radians(lat1)
    lat2_r = radians(lat2)
    lat_delta = radians(lat2 - lat1)
    lon_delta = radians(lon2 - lon1)

    hav1 = sin(lat_delta / 2)**2
    hav2 = sin(lon_delta / 2)**2

    a = asin(sqrt(hav1 + cos(lat1_r) * cos(lat2_r) * hav2))
    return 2 * r * a


def fast_round(num, precision=1):
    p = 10**precision
    return int(num * p + 0.5) / p
