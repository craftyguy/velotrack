import pytest
import ridecasa
import ridecasa.models


@pytest.fixture
def client():
    flask_app = ridecasa.create_testing_app('ridecasa.config.TestConfig')
    with flask_app.test_client() as client:
        yield client


@pytest.fixture
def init_db():
    db = ridecasa.models.db
    ridecasa.models.init_db(':memory:')

    yield db


@pytest.fixture
def auth_client():
    flask_app = ridecasa.create_testing_app('ridecasa.config.TestConfig')
    with flask_app.test_client() as client:
        client.post('/login',
                    data=dict(
                        username='test_user',
                        password='testpassword123!@#',
                        submit=True),
                    follow_redirects=True)
        yield client
