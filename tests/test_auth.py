from pony import orm
from pony.orm import db_session
from ridecasa.models import db


def test_logged_out_home(client):
    resp = client.get('/', follow_redirects=True)
    assert b'Username' in resp.data
    assert b'Password' in resp.data
    assert b'Log in' in resp.data
    assert resp.status_code == 200


def test_logged_out_settings(client):
    resp = client.get('/settings', follow_redirects=True)
    assert b'Page not found' in resp.data
    assert resp.status_code == 404


def test_logged_out_unshared_activity(client):
    resp = client.get('/activity/1', follow_redirects=True)
    assert b'Please log in to access this page' in resp.data
    assert resp.status_code == 200


def test_logged_out_messages(client):
    resp = client.get('/messages', follow_redirects=True)
    assert b'Please log in to access this page' in resp.data
    assert resp.status_code == 200


@db_session
def test_signup(client, init_db):
    resp = client.post('/signup',
                       data=dict(
                           username='test_user',
                           password='testpassword123!@#',
                           repassword='testpassword123!@#',
                           email='test@example.com',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'No activies here' in resp.data
    user = orm.select(u for u in db.User).first()
    assert user is not None
    assert user.username == 'test_user'
    assert user.password != 'testpassword123!@#'


def test_signup_existing(client, init_db):
    resp = client.post('/signup',
                       data=dict(
                           username='test_user',
                           password='testpassword123!@#',
                           repassword='testpassword123!@#',
                           email='test@example.com',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert (b'Username &#39;test_user&#39; is already taken, '
            b'please choose another.') in resp.data


def test_signup_no_email(client, init_db):
    resp = client.post('/signup',
                       data=dict(
                           username='test_user2',
                           password='testpassword123!@#',
                           repassword='testpassword123!@#',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'Invalid e-mail address' in resp.data


def test_signup_existing_email(client, init_db):
    resp = client.post('/signup',
                       data=dict(
                           username='test_user2',
                           password='testpassword123!@#',
                           repassword='testpassword123!@#',
                           email='test@example.com',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'email is already registered' in resp.data


def test_login_logout(client, init_db):
    resp = client.post('/login',
                       data=dict(
                           username='test_user',
                           password='testpassword123!@#',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'No activies here' in resp.data
    resp = client.get('/logout', follow_redirects=True)
    assert resp.status_code == 200
    assert b'Log in' in resp.data


def test_invalid_password(client, init_db):
    resp = client.post('/login',
                       data=dict(
                           username='test_user',
                           password='lolwrong',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'Invalid username or password' in resp.data


def test_no_password(client, init_db):
    resp = client.post('/login',
                       data=dict(
                           username='test_user',
                           password='',
                           submit=True),
                       follow_redirects=True)
    assert resp.status_code == 200
    assert b'Password required' in resp.data
