import pytest
from ridecasa import importer
from ridecasa.sportdata import ActivityData


def test_importer_valid_fit():
    i = importer.Importer('tests/data/tiny.fit')
    contents = i.import_file()
    assert isinstance(contents[0][0], ActivityData)


def test_importer_invalid_fit():
    i = importer.Importer('tests/data/jamestown_broken.fit')
    with pytest.raises(importer.ActivityImportError) as e_info:
        i.import_file()
        assert '"Error parsing file' in e_info.value[0]


def test_importer_invalid_filetype():
    i = importer.Importer('dummy.xyz')
    with pytest.raises(importer.ActivityImportError) as e_info:
        i.import_file()
        assert 'Unsupported extension' in e_info.value[0]
