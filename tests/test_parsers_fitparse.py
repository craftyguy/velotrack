import datetime
import pytest
from ridecasa import parsers


def test_fitparser_jamestown_broken():
    parser = parsers.FITParse()
    with pytest.raises(parsers.ParseError):
        parser.parse('tests/data/jamestown_broken.fit')


def test_fitparser_jamestown():
    parser = parsers.FITParse()
    ret = parser.parse('tests/data/jamestown.fit')
    assert ret
    assert ret[0].distanceMeters == 48770.66
    assert ret[0].avg_hr == 156
    assert ret[0].startTime == datetime.datetime(2020, 6, 12, 9, 40, 51)
    assert ret[0].timezone == 'America/New_York'
    assert ret[0].calories == 2190
