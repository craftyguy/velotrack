import datetime
from ridecasa import parsers


def test_tcxparser():
    parser = parsers.TCXParse()
    ret = parser.parse('tests/data/jamestown.tcx')
    assert ret is not None
    assert ret[0].distanceMeters == 48928.68
    assert ret[0].avg_hr == 156
    assert ret[0].startTime == datetime.datetime(2020, 6, 12, 9, 40, 51)
    assert ret[0].timezone == 'America/New_York'
