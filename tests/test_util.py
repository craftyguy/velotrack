import pytest
from ridecasa import util


def test_util_fast_round():
    assert 10.1 == util.fast_round(10.123456)
    assert 10.1 == util.fast_round(10.123456, 1)
    assert 10.12 == util.fast_round(10.123456, 2)
    assert 10.1235 == util.fast_round(10.123456, 4)
    assert 10.12346 == util.fast_round(10.123456, 5)
    assert 10.123456 == util.fast_round(10.123456, 6)


def test_util_geohash_encode():
    assert util.geohash_encode(16.1039, -86.8984) == 'd4f5weds0889'


def test_util_geohash_encode_fail():
    with pytest.raises(util.GeohashEncodeException) as e_info:
        util.geohash_encode(':(', ':D')
        assert 'Failed to encode geohash' in e_info.value[0]


def test_util_geohash_decode():
    assert util.geohash_decode('d4f5weds0889') == (16.103900028392673,
                                                   -86.8983999453485)


def test_util_geohash_decode_fail():
    with pytest.raises(util.GeohashDecodeException) as e_info:
        util.geohash_decode(':(')
        assert 'Failed to decode geohash' in e_info.value[0]


def test_util_remove_parens():
    assert util.remove_parens('(foo) bar') == 'foo bar'
    assert util.remove_parens('()()()()()()()()') == ''


@pytest.mark.parametrize("latlon1, latlon2, distance",
                         [((36.12, -86.67),
                           (33.94, -118.40),
                           2886444.4428379834),
                          ((37.22614288330078, -76.78239441476762),
                           (37.22643658518791, -76.78175353445113),
                           65.4700621793856)
                          ])
def test_util_haversine_distance_meters(latlon1, latlon2, distance):
    assert util.haversine_distance_meters(latlon1, latlon2) == distance
